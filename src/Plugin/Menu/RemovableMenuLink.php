<?php

namespace Drupal\profile_manager\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;

/**
 * A class for allowing simple links to be deleted if necessary.
 *
 * There are some simple menu items that profile_manager adds, and they may
 * need to be removed or rebuilt based upon user interactions. This class
 * allows for them to be removed and updated as necessary.
 *
 * Since we have no storage considerations beyond the treeStorage used within
 * core, we just need to mark it as deletable.
 */
class RemovableMenuLink extends MenuLinkDefault {

  /**
   * {@inheritdoc}
   */
  protected $overrideAllowed = [
    'menu_name' => 1,
    'parent' => 1,
    'weight' => 1,
    'expanded' => 1,
    'enabled' => 1,
    'title' => 1,
  ];

  /**
   * {@inheritdoc}
   */
  public function isDeletable() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLink() {
  }

}
