<?php

namespace Drupal\profile_manager\Plugin\ProfileManager;

use Symfony\Component\Routing\Route;

/**
 * @ProfileManager(
 *   id = "site_section",
 *   route = "system.admin_structure",
 *   title = @Translation("Settings"),
 *   description = @Translation("Control high level site configuration settings."),
 *   weight = 11,
 * )
 */
class SiteSection extends ProfileManagerBase {

  /**
   * {@inheritdoc}
   */
  public function getTransformedRouteName(): string {
    return 'profile_manager.site_section';
  }

  protected function getRouteDefaults(Route $route): array {
    $defaults = parent::getRouteDefaults($route);
    $defaults['_controller'] = '\Drupal\profile_manager\Controller\BlockContents::getBlockContents';
    $defaults['menu_name'] = 'profile-manager';
    return $defaults;
  }


  public function getRouteRequirements(Route $route): array {
    return ['_profile_manager_access_admin_overview_page' => TRUE];
  }

  /**
   * {@inheritdoc}
   */
  protected function getRoutePath(Route $route): string {
    $config = $this->getConfiguration();
    return $config['path_prefix'] . '/settings';
  }

}
