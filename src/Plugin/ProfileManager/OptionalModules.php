<?php

namespace Drupal\profile_manager\Plugin\ProfileManager;

use Symfony\Component\Routing\Route;

/**
 * @ProfileManager(
 *   id = "add_ons",
 *   route = "profile_manager.optional_modules",
 *   title = @Translation("Add-ons"),
 *   description = @Translation("Add additional functionality to your site."),
 *   weight = 10,
 * )
 */
class OptionalModules extends ProfileManagerBase {

  /**
   * {@inheritdoc}
   */
  public function getTransformedRouteName(): string {
    return 'profile_manager.add_ons';
  }

  /**
   * {@inheritdoc}
   */
  protected function getRoutePath(Route $route): string {
    $config = $this->getConfiguration();
    return $config['path_prefix'] . '/addons';
  }

}
