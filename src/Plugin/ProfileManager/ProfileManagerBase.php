<?php

namespace Drupal\profile_manager\Plugin\ProfileManager;

use Drupal\Core\Plugin\PluginBase;
use Drupal\profile_manager\ProfileManagerInterface;
use Symfony\Component\Routing\Route;

abstract class ProfileManagerBase extends PluginBase implements ProfileManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function transformRoute(Route $route): Route {
    return new Route(
      $this->getRoutePath($route),
      $this->getRouteDefaults($route),
      $this->getRouteRequirements($route),
      $this->getRouteOptions($route),
      $this->getRouteHost($route),
      $this->getRouteSchemes($route),
      $this->getRouteMethods($route),
      $this->getRouteCondition($route)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getTransformedRouteName(): string {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return 'profile_manager.' . $definition->getRouteName();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(Route $route): string {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    $title = $definition->getDefinition()['title'] ?? $route->getDefault('_title');
    return $title ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(Route $route): string {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return $definition->getDefinition()['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkParent(): string {
    return '';
  }

  /**
   * Helper function to custom define a profile_manager route's path.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return string
   */
  protected function getRoutePath(Route $route): string {
    $config = $this->getConfiguration();
    return $config['path_prefix'] . $route->getPath();
  }

  /**
   * Helper function to custom define a profile_manager route's defaults.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return array
   */
  protected function getRouteDefaults(Route $route): array {
    $defaults = $route->getDefaults();
    $defaults['_title'] = $this->getTitle($route);
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return !empty($definition->getDefinition()['defaults']) ? $definition->getDefinition()['defaults'] + $defaults : $defaults;
  }

  /**
   * Helper function to custom define a profile_manager route's requirements.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return array
   */
  protected function getRouteRequirements(Route $route): array {
    $requirements = $route->getRequirements();
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return !empty($definition->getDefinition()['requirements']) ? $definition->getDefinition()['requirements'] + $requirements : $requirements;
  }

  /**
   * Helper function to custom define a profile_manager route's options.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return array
   */
  protected function getRouteOptions(Route $route): array {
    $options = $route->getOptions();
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return !empty($definition->getDefinition()['options']) ? $definition->getDefinition()['options'] + $options : $options;
  }

  /**
   * Helper function to custom define a profile_manager route's host.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return string
   */
  protected function getRouteHost(Route $route): string {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return $definition->getDefinition()['host'] ?? $route->getHost();
  }

  /**
   * Helper function to custom define a profile_manager route's schemes.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return string[]
   */
  protected function getRouteSchemes(Route $route): array {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return $definition->getDefinition()['schemes'] ?? $route->getSchemes();
  }

  /**
   * Helper function to custom define a profile_manager route's methods.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return string[]
   */
  protected function getRouteMethods(Route $route): array {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return $definition->getDefinition()['methods'] ?? $route->getMethods();
  }

  /**
   * Helper function to custom define a profile_manager route's condition.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The original route we are copying.
   *
   * @return string
   */
  protected function getRouteCondition(Route $route): string {
    /** @var \Drupal\profile_manager\Annotation\ProfileManagerDefinition $definition */
    $definition = $this->getPluginDefinition();
    return $definition->getDefinition()['condition'] ?? $route->getCondition();
  }

}
