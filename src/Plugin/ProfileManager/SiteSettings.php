<?php

namespace Drupal\profile_manager\Plugin\ProfileManager;

use Symfony\Component\Routing\Route;

/**
 * @ProfileManager(
 *   id = "site_settings",
 *   route = "system.site_information_settings",
 *   title = @Translation("Site"),
 *   weight = -10,
 *   description = @Translation("Change site name, email address, slogan, default front page, and error pages.")
 * )
 */
class SiteSettings extends ProfileManagerBase {

  /**
   * @return string
   */
  public function getLinkParent(): string {
    return 'profile_manager.site_section';
  }

  /**
   * @param \Symfony\Component\Routing\Route $route
   *
   * @return array
   */
  protected function getRouteRequirements(Route $route): array {
    return [
      '_permission' => 'access profile manager site settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getRoutePath(Route $route): string {
    $config = $this->getConfiguration();
    return $config['path_prefix'] . '/settings/site';
  }

}
