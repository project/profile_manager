<?php

namespace Drupal\profile_manager\Annotation;

use Drupal\Component\Annotation\AnnotationBase;
use Drupal\Core\Annotation\Translation;
use Symfony\Component\Routing\Route;

/**
 * @Annotation
 */
class ProfileManagerRoute extends AnnotationBase {

  /**
   * The path partial to be used when profile_manager creates this route.
   *
   * @var string
   */
  public $path;

  /**
   * The title to associate with the route path.
   *
   * @var Translation
   */
  public $title;

  /**
   * @var \Symfony\Component\Routing\Route
   */
  protected $route;

  public function __construct(array $values) {
    $this->route = new Route(
      $values['path'],
      ['_title' => $values['title']],
    );
  }

  public function get() {
    return $this->route;
  }

}
