<?php

namespace Drupal\profile_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * @Annotation
 */
class ProfileManager extends Plugin {

  public function get() {
    $plugin_id = $this->definition['id'];
    $route = $this->definition['route'];
    $class = $this->definition['class'];
    $provider = $this->definition['provider'];
    unset($this->definition['id'], $this->definition['route'], $this->definition['class'], $this->definition['provider']);
    return new ProfileManagerDefinition($plugin_id, $route, $class, $provider, $this->definition);
  }

}
