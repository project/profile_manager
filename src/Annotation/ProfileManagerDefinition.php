<?php

namespace Drupal\profile_manager\Annotation;

use Drupal\Component\Plugin\Definition\DerivablePluginDefinitionInterface;
use Drupal\Component\Plugin\Definition\PluginDefinitionInterface;
use Drupal\Component\Plugin\Derivative\DeriverInterface;
use Symfony\Component\Routing\Route;

class ProfileManagerDefinition implements PluginDefinitionInterface, DerivablePluginDefinitionInterface {

  /**
   * The id of the plugin.
   *
   * @var string
   */
  protected $pluginId;

  /**
   * The route name that will be transformed.
   *
   * @var string
   */
  protected $routeName;

  /**
   * The class name of the plugin.
   *
   * @var string
   */
  protected $class;

  /**
   * The module/library which provided the plugin.
   *
   * @var string
   */
  protected $provider;

  /**
   * Any other values passed with the plugin.
   *
   * @var array
   */
  protected $definition;

  /**
   * ProfileManagerDefinition constructor.
   */
  public function __construct(string $plugin_id, $route_name, string $class, string $provider, array $definition) {
    $this->pluginId = $plugin_id;
    $this->routeName = $route_name;
    $this->class = $class;
    $this->provider = $provider;
    $this->definition = $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->pluginId;
  }

  /**
   * @return Route
   */
  public function getRouteName(): string {
    return $this->routeName;
  }

  /**
   * {@inheritdoc}
   */
  public function getClass(): string {
    return $this->class;
  }

  /**
   * {@inheritdoc}
   */
  public function setClass($class): self {
    if (class_exists($class)) {
      $this->class = $class;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getProvider(): string {
    return $this->provider;
  }

  /**
   * @return array
   */
  public function getDefinition(): array {
    return $this->definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getDeriver() {
    return $this->getDefinition()['deriver'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDeriver($deriver) {
    if (class_exists($deriver) && array_key_exists(DeriverInterface::class, class_implements($deriver))) {
      $this->definition['deriver'] = $deriver;
    }
    return $this;
  }

}
