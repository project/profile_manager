<?php

namespace Drupal\profile_manager\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\profile_manager\Annotation\ProfileManagerDefinition;
use Drupal\profile_manager\Plugin\Menu\RemovableMenuLink;
use Drupal\profile_manager\ProfileManagerInterface;
use Drupal\profile_manager\ProfileManagerPluginManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for profile_manager routes.
 *
 * @internal
 *   Tagged services are internal.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The profile manager plugin manager.
   *
   * @var ProfileManagerPluginManager
   */
  protected $profileManagerPluginManager;

  /**
   * The menu link manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $linkManager;

  /**
   * The profile manager configuration settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The list of plugins for which we'll delay because they depend on another.
   *
   * @var array
   */
  protected $delayedPlugins = [];

  /**
   * RouteSubscriber constructor.
   *
   * @param ProfileManagerPluginManager $profile_manager
   *   The plugin manager that controls profile manager route transformations.
   */
  public function __construct(ProfileManagerPluginManager $profile_manager, MenuLinkManagerInterface $link_manager, ConfigFactoryInterface $factory) {
    $this->profileManagerPluginManager = $profile_manager;
    $this->linkManager = $link_manager;
    $this->config = $factory->get('profile_manager.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // After Layout Builder override routes have been built.
    $events[RoutingEvents::ALTER][] = ['onAlterRoutes', -120];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if (!$this->config->get('path_prefix') || !$this->config->get('use_profile_manager')) {
      return;
    }
    $this->setupToolbarSubtrees($collection);
    $this->setupOverview($collection);
    if ($this->config->get('allowed_modules')) {
      $this->setupAvailableModules($collection);
    }
    // @todo add the option modules enable form route.
    /** @var ProfileManagerDefinition $plugin_definition */
    // @todo move this loop to config entities that store route access config for the plugin instances.
    $definitions = $this->profileManagerPluginManager->getDefinitions();
    foreach ($definitions as $plugin_definition) {
      $module_route = $collection->get($plugin_definition->getRouteName());
      if (!$module_route) {
        continue;
      }
      $this->setupPlugin($collection, $plugin_definition, $module_route);
    }
  }

  protected function setupToolbarSubtrees(RouteCollection $collection) {
    $route = $collection->get('toolbar.subtrees');
    $requirements = $route->getRequirements();
    $requirements['_custom_access'] = "\Drupal\profile_manager\ToolbarController::checkSubTreeAccess";
    $route->setRequirements($requirements);
  }

  protected function setupOverview(RouteCollection $collection) {
    $route = new Route(
      $this->config->get('path_prefix'),
      [
        '_controller' => '\Drupal\system\Controller\SystemController::overview',
        '_title' => $this->config->get('label'),
      ],
      [
        '_permission' => 'access profile manager overview'
      ]
    );
    $collection->add('profile_manager.overview', $route);
    $link = [
      'class' => RemovableMenuLink::class,
      'title' => $this->config->get('label'),
      'route_name' => 'profile_manager.overview',
      'parent' => NULL,
      'menu_name' => 'profile-manager',
      'provider' => 'profile_manager',
    ];
    if ($this->linkManager->hasDefinition('profile_manager.overview')) {
      $this->linkManager->updateDefinition('profile_manager.overview', $link);
    }
    else {
      $this->linkManager->addDefinition('profile_manager.overview', $link);
    }
  }

  protected function setupAvailableModules(RouteCollection $collection) {
    $route = new Route(
      $this->config->get('path_prefix') . '/modules',
      [
        '_form' => '\Drupal\profile_manager\Form\ProfileManagerInstallOptionalModulesForm',
        '_title' => 'Optional Modules',
      ],
      [
        '_permission' => 'access profile manager modules'
      ]
    );
    $collection->add('profile_manager.optional_modules', $route);
  }

  protected function setupPlugin(RouteCollection $collection, ProfileManagerDefinition $plugin_definition, Route $module_route) {
    /** @var ProfileManagerInterface $plugin */
    $plugin = $this->profileManagerPluginManager->createInstance($plugin_definition->id());
    $parent = $plugin->getLinkParent();
    if ($parent && !$this->linkManager->hasDefinition($parent)) {
      $this->setDelayedPlugin($parent, $plugin_definition, $module_route);
      return;
    }

    $route = $plugin->transformRoute($module_route);
    $collection->add($plugin->getTransformedRouteName(), $route);

    if (empty($parent)) {
      $parent = 'profile_manager.overview';
    }
    if (empty($plugin_definition->getDefinition()['no_link'])) {
      $link = [
        'class' => RemovableMenuLink::class,
        'title' => $plugin->getTitle($module_route),
        'description' => $plugin->getDescription($module_route),
        'route_name' => $plugin->getTransformedRouteName(),
        'weight' => $plugin_definition->getDefinition()['weight'] ?? 0,
        'menu_name' => 'profile-manager',
        'parent' => $parent,
        'provider' => 'profile_manager',
      ];
      if (!empty($plugin_definition->getDefinition()['route_parameters'])) {
        $link['route_parameters'] = $plugin_definition->getDefinition()['route_parameters'];
      }
      if ($this->linkManager->hasDefinition($plugin->getTransformedRouteName())) {
        $this->linkManager->updateDefinition($plugin->getTransformedRouteName(), $link);
      }
      else {
        $this->linkManager->addDefinition($plugin->getTransformedRouteName(), $link);
      }
    }
    foreach ($this->getDelayedPlugin($plugin->getTransformedRouteName()) as $plugin_data) {
      $this->setupPlugin($collection, $plugin_data['definition'], $plugin_data['route']);
    }
  }

  protected function setDelayedPlugin(string $parent, ProfileManagerDefinition $definition, Route $module_route) {
    $this->delayedPlugins[$parent][$definition->id()] = [
      'definition' => $definition,
      'route' => $module_route,
    ];
  }

  protected function getDelayedPlugin(string $parent) {
    return $this->delayedPlugins[$parent] ?? [];
  }

}
