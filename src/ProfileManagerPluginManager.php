<?php

namespace Drupal\profile_manager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class ProfileManagerPluginManager extends DefaultPluginManager {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactory $config_factory) {
    parent::__construct('Plugin/ProfileManager', $namespaces, $module_handler, '\Drupal\profile_manager\ProfileManagerInterface', '\Drupal\profile_manager\Annotation\ProfileManager');

    $this->alterInfo('profile_manager');
    $this->setCacheBackend($cache_backend, 'profile_manager_plugins');
    $this->config = $config_factory->get('profile_manager.settings');
  }

  public function getDefinitions() {
    // If there's no path_prefix set, we're not doing anything, so don't return a list of plugins.
    // @todo consider moving this to a plugin filter.
    if (!$this->config->get('path_prefix')) {
      return [];
    }
    $definitions = parent::getDefinitions();

    $allowed_modules = $this->config->get('allowed_modules') ?? [];
    $allowed_modules += ['profile_manager'];
    // Only return plugins that are members of modules on the allow list.
    // @todo consider moving this to a plugin filter.
    array_filter($definitions, function ($definition) use ($allowed_modules) {
      return in_array($definition->getProvider(), $allowed_modules);
    });

    return $definitions;
  }

  public function createInstance($plugin_id, array $configuration = []) {
    return parent::createInstance($plugin_id, array_merge($this->config->getRawData(), $configuration));
  }

}
