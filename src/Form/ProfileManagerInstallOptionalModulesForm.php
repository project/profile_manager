<?php

namespace Drupal\profile_manager\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProfileManagerInstallOptionalModulesForm extends FormBase {

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The list of available extensions.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensions;

  /**
   * The array of allowed modules.
   *
   * @var array|null
   */
  protected $allowedModules = [];

  public function __construct(ModuleInstallerInterface $module_installer, ModuleHandlerInterface $module_handler, ExtensionList $extensions, ConfigFactoryInterface $config_factory) {
    $this->moduleInstaller = $module_installer;
    $this->moduleHandler = $module_handler;
    $this->extensions = $extensions;
    $this->allowedModules = $config_factory->get('profile_manager.settings')->get('allowed_modules');
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_installer'),
      $container->get('module_handler'),
      $container->get('extension.list.module'),
      $container->get('config.factory')
    );
  }


  /**
   * @return string
   */
  public function getFormId() {
    return 'profile_manager_install_optional_modules';
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['allowed_modules']['#tree'] = TRUE;
    foreach (array_keys($this->allowedModules) as $module_name) {
      $extension = $this->extensions->get($module_name);
      $package = $extension->info['package'] ?? 'Other';
      if (empty($form['allowed_modules'][$package])) {
        $form['allowed_modules'][$package] = [
          '#type' => 'fieldset',
          '#title' => $package,
        ];
      }
      $form['allowed_modules'][$package][$module_name] = [
        '#type' => 'checkbox',
        '#title' => $extension->info['name'],
        '#description' => $extension->info['description'],
        '#default_value' => $this->moduleHandler->moduleExists($module_name),
        '#disabled' => $this->moduleHandler->moduleExists($module_name),
      ];
    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
    ];

    // By default, render the form using system-config-form.html.twig.
    $form['#theme'] = 'system_config_form';
    return $form;
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $install = [];
    foreach ($form_state->getValue('allowed_modules') as $modules) {
      $modules = array_filter($modules);
      foreach (array_keys($modules) as $module_name) {
        if (!$this->moduleHandler->moduleExists($module_name)) {
          $install[] = $module_name;
        }
      }
    }
    if (!$install) {
      return;
    }
    $this->moduleInstaller->install($install);
  }


}
