<?php

namespace Drupal\profile_manager\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProfileManagerModulesForm extends ConfigFormBase {

  const MODULE_EXCLUDE_CONFIG_KEY = 'allowed_admin_modules';

  const MODULE_INCLUDE_CONFIG_KEY = 'allowed_modules';

  /**
   * The module handler.
   *
   * @var ModuleHandlerInterface
   */
  protected $handler;

  /**
   * The name of the install profile.
   *
   * @var string
   */
  protected $profile;

  /**
   * @var ExtensionList
   */
  protected $extensions;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->getParameter('install_profile'),
      $container->get('extension.list.module')
    );
  }

  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, string $profile, ExtensionList $extensions) {
    parent::__construct($config_factory);
    $this->handler = $module_handler;
    $this->extensions = $extensions;
    $this->profile = $profile;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['profile_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_manager_modules';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['description'] = [
      '#markup' => '<p>' . $this->t('Modules selected below will become available for site owners to enable on their own sites. Choosing any module below will not automatically enable it. That behavior can be found at the <a href="/admin/modules">Module Extensions</a> page.') . '</p>',
    ];
    $extensions = $this->getAvailableExtensionsList();
    $form['allowed_modules']['#tree'] = TRUE;
    $config = $this->config('profile_manager.settings');
    foreach ($extensions as $package => $modules) {
      $form['allowed_modules'][$package] = [
        '#type' => 'fieldset',
        '#title' => $package,
      ];
      foreach ($modules as $module_name => $extension) {
        if (!$config->get(static::MODULE_EXCLUDE_CONFIG_KEY . ".$module_name")) {
          $form['allowed_modules'][$package][$module_name] = [
            '#type' => 'checkbox',
            '#title' => $extension->info['name'],
            '#description' => $extension->info['description'],
            '#default_value' => $config->get(static::MODULE_INCLUDE_CONFIG_KEY . ".$module_name") ?? FALSE,
          ];
        }
      }
    }
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $module_list = $form_state->getValue('allowed_modules');
    $allowed_modules = [];
    foreach ($module_list as $package => $modules) {
      $allowed_modules += array_filter($modules);
    }
    $config = $this->config('profile_manager.settings');
    $config->set(static::MODULE_INCLUDE_CONFIG_KEY, $allowed_modules);
    $config->save();
    parent::submitForm($form, $form_state);
  }

  protected function getAvailableExtensionsList(): array {
    $extensions = $this->extensions->getList();
    $this->filterExtensions($extensions);
    $extensions = $this->sortExtensions($extensions);
    return $extensions;
  }

  protected function getProfileModules(array $info, string $path) {
    // @todo figure out how to handle module dependencies vs config_install
    $modules = ['module' => []];
    if (!empty($info['config_install']) && file_exists($path . DIRECTORY_SEPARATOR . 'config/sync/core.extension.yml')) {
      $modules = Yaml::decode(file_get_contents($path . DIRECTORY_SEPARATOR . 'config/sync/core.extension.yml'));
    }
    return $modules;
  }

  protected function filterExtensions(array &$extensions) {
    $profile = $this->handler->getModule($this->profile);
    $path = realpath($profile->getPath());
    $info = Yaml::decode(file_get_contents($path . DIRECTORY_SEPARATOR . $profile->getFilename()));
    $profile_modules = $this->getProfileModules($info, $path);
    foreach (array_keys($profile_modules['module']) as $module_name) {
      // Remove required profile modules from the list of potential candidates.
      unset($extensions[$module_name]);
    }
    foreach ($extensions as $module_name => $extension) {
      $package = $extension->info['package'] ?? '';
      // Remove testing modules from the list of potential candidates.
      if ($package === "Testing") {
        unset($extensions[$module_name]);
      }
      // Remove hidden modules from the list of potential candidates.
      if (!empty($extension->info['hidden']) && $extension->info['hidden']) {
        unset($extensions[$module_name]);
      }
    }
  }

  protected function sortExtensions(array $extensions): array {
    $modules = [];
    // Sort modules into their packages for display.
    foreach ($extensions as $module_name => $extension) {
      $package = $extension->info['package'] ?? 'Other';
      $modules[$package][$module_name] = $extension;
    }
    // Sort the package names alphabetically.
    ksort($modules);
    return $modules;
  }

}
