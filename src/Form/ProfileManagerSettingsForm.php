<?php

namespace Drupal\profile_manager\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class ProfileManagerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['profile_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'profile_manager_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['use_profile_manager'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Profile Manager functionality.'),
      '#description' => $this->t('The turns the toolbar and menu elements on/off.'),
      '#default_value' => $this->config('profile_manager.settings')->get('use_profile_manager') ?? FALSE,
    ];
    $form['path_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path prefix'),
      '#description' => $this->t('The prefix to use for profile_manager paths.'),
      '#default_value' => $this->config('profile_manager.settings')->get('path_prefix') ?? '',
      '#states' => [
        'visible' => [
          ':input[name="use_profile_manager"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="use_profile_manager"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('The label used in the toolbar admin.'),
      '#default_value' => $this->config('profile_manager.settings')->get('label') ?? '',
      '#states' => [
        'visible' => [
          ':input[name="use_profile_manager"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="use_profile_manager"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['toolbar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add Toolbar item.'),
      '#description' => $this->t('Creates a top-level item in the toolbar instead of nesting administration within the "Manage" menu.'),
      '#default_value' => $this->config('profile_manager.settings')->get('toolbar') ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="use_profile_manager"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['toolbar_home'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Reposition "Back to Site".'),
      '#description' => $this->t('Move "Back to Site" toolbar item into Profile Manager tool tray.'),
      '#default_value' => $this->config('profile_manager.settings')->get('toolbar_home') ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="toolbar"]' => ['checked' => TRUE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('use_profile_manager')) {
      return;
    }

    $path = ltrim($form_state->getValue('path_prefix'), '/');

    $parsed_url = UrlHelper::parse($path);
    if (!empty($parsed_url['query'])) {
      $form_state->setError($form['path_prefix'], $this->t('Query parameters are not supported for the path prefix.'));
    }
    if (!empty($parsed_url['fragment'])) {
      $form_state->setError($form['path_prefix'], $this->t('Fragments are not supported for the path prefix.'));
    }
    if ($parsed_url['path'] == '<front>') {
      $form_state->setError($form['path_prefix'], $this->t('The path prefix may not be the front page.'));
    }
    if ($parsed_url['path'] == '<none>') {
      $form_state->setError($form['path_prefix'], $this->t('The path prefix may not be set to <none>.'));
    }
    if (UrlHelper::isExternal($path)) {
      $form_state->setError($form['path_prefix'], $this->t('The path prefix may not be an external url.'));
    }
    if (!UrlHelper::isValid($path)) {
      $form_state->setError($form['path_prefix'], $this->t('The path prefix must be a valid url.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('profile_manager.settings');
    $config->set('use_profile_manager', $form_state->getValue('use_profile_manager'));
    $config->set('path_prefix', $form_state->getValue('path_prefix'));
    $config->set('label', $form_state->getValue('label'));
    $config->set('toolbar', $form_state->getValue('toolbar'));
    $config->set('toolbar_home', $form_state->getValue('toolbar') ? $form_state->getValue('toolbar_home') : FALSE);
    $config->save();
    parent::submitForm($form, $form_state);
    drupal_flush_all_caches();
  }

}
