<?php

namespace Drupal\profile_manager\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\profile_manager\Form\ProfileManagerModulesForm;

class ProfileManagerAdminModulesForm extends ProfileManagerModulesForm {

  const MODULE_EXCLUDE_CONFIG_KEY = 'allowed_modules';

  const MODULE_INCLUDE_CONFIG_KEY = 'allowed_admin_modules';

  public function getFormId() {
    return 'profile_manager_admin_modules';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['description']['#markup'] = '<p>' . $this->t('Modules selected below will be allowed to be turned on by administrative users (who have access to the <a href="/admin/modules">Module Extensions</a> page) and will not be turned off during a config sync operation.') . '</p>';
    return $form;
  }

}
