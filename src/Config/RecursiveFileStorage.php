<?php

namespace Drupal\profile_manager\Config;

use Drupal\Core\Config\FileStorage;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class RecursiveFileStorage extends FileStorage {

  protected array $files;

  /**
   * {@inheritdoc}
   */
  public function listAll($prefix = '') {
    if (empty($this->files)) {
      $dir = $this->directory;
      if (!is_dir($dir)) {
        return [];
      }

      $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
      foreach ($iterator as $file) {
        if ($file->isDir()) {
          continue;
        }
        $info = pathinfo($file->getFilename());
        $this->files[$info['filename']] = $file;
      }
    }

    $names = [];
    if ($prefix) {
      $extension = '.' . $this->getFileExtension();
      $pattern = '/^' . preg_quote($prefix, '/') . '.*' . preg_quote($extension, '/') . '$/';
      foreach ($this->files as $name => $file) {
        if (preg_match($pattern, $file->getFilename())) {
          $names[] = $name;
        }
      }
    }

    return !empty($names) ? $names : array_keys($this->files);
  }

  public function getFilePath($name) {
    if (empty($this->files)) {
      $this->listAll();
    }
    return !empty($this->files[$name]) ? $this->files[$name]->getPathname() : '';
  }

}
