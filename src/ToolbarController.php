<?php

namespace Drupal\profile_manager;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;

class ToolbarController extends ControllerBase {

  /**
   * Checks access for the subtree controller.
   *
   * @param string $hash
   *   The hash of the toolbar subtrees.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkSubTreeAccess($hash) {
    $expected_hash = _toolbar_get_subtrees_hash()[0];
    $pm_hash = _profile_manager_get_subtrees_hash()[0];
    return AccessResult::allowedIf($this->currentUser()->hasPermission('access toolbar') && (hash_equals($expected_hash, $hash) || hash_equals($pm_hash, $hash)))->cachePerPermissions();
  }

}
