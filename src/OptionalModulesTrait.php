<?php

namespace Drupal\profile_manager;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Serialization\Yaml;

/**
 * Provides simplified access to optional module details like name and path.
 */
trait OptionalModulesTrait {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $profileManagerConfig;

  /**
   * The module handler object.
   *
   * @var ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $handler;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected ExtensionPathResolver $extensionPathResolver;

  /**
   * The name of the install profile.
   *
   * @var string
   */
  protected string $installProfile;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $handler
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extension_path_resolver
   * @param string $install_profile
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $handler, ExtensionPathResolver $extension_path_resolver, string $install_profile) {
    $this->profileManagerConfig = $config_factory->get('profile_manager.settings');
    $this->handler = $handler;
    $this->extensionPathResolver = $extension_path_resolver;
    $this->installProfile = $install_profile;
  }

  /**
   * Get a list of enabled "allowed_modules" directory paths.
   *
   * @return array
   */
  protected function getProfileOptionalModulePaths(): array {
    $paths = [];
    $allowed_modules = $this->profileManagerConfig->get('allowed_modules') ?? [];
    $allowed_modules += $this->profileManagerConfig->get('allowed_admin_modules') ?? [];
    foreach (array_keys($allowed_modules) as $module_name) {
      if (!$this->handler->moduleExists($module_name)) {
        continue;
      }
      $extension = $this->handler->getModule($module_name);
      $paths[$module_name] = realpath($extension->getPath());
    }
    return $paths;
  }

  protected function getProfileOptionalModuleDependencyPaths(array $modules = [], array &$processed = []): array {
    if (empty($modules)) {
      $modules = $this->getProfileOptionalModulePaths();
    }
    $module_infos = [];
    foreach ($modules as $module_name => $path) {
      $module_infos[] = Yaml::decode(file_get_contents($path . DIRECTORY_SEPARATOR . "$module_name.info.yml"));
    }
    $module_paths = [];
    foreach ($module_infos as $module_info) {
      // If the module has no dependencies, move on to the next optional module.
      if (empty($module_info['dependencies'])) {
        continue;
      }
      foreach ($module_info['dependencies'] as $module) {
        [, $module] = explode(':', $module);
        // Get module paths so we can check its config.
        $extension = $this->handler->getModule($module);
        $module_paths[$module] = realpath($extension->getPath());
      }
    }
    if (array_diff_key($module_paths, $processed)) {
      $processed = NestedArray::mergeDeep($processed, $module_paths);
      $processed = $this->getProfileOptionalModuleDependencyPaths($module_paths, $processed);
    }
    return $processed;
  }

  /**
   * Get a list of enabled "allowed_modules" present in the codebase.
   *
   * @return array
   */
  protected function getProfileOptionalModules(): array {
    $modules = [];
    $allowed_modules = $this->profileManagerConfig->get('allowed_modules') ?? [];
    $allowed_modules += $this->profileManagerConfig->get('allowed_admin_modules') ?? [];
    foreach (array_keys($allowed_modules) as $module_name) {
      if (!$this->handler->moduleExists($module_name)) {
        continue;
      }
      // @todo handle module weight?
      // Since this is going to be used to add/remove from core.extension
      // during the import/export process, we should find and preserve module
      // weight.
      $modules[] = $module_name;
    }
    return $modules;
  }

}
