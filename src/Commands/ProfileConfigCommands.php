<?php

namespace Drupal\profile_manager\Commands;

use Consolidation\AnnotatedCommand\Events\CustomEventAwareInterface;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareTrait;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\Extension;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Serialization\Yaml;
use Drush\Commands\DrushCommands;
use Drush\SiteAlias\SiteAliasManagerAwareInterface;

/**
 * Drush commands for exporting and deploying sites using profile manager.
 *
 * The deploy command is left here for legacy reasons. Drush's "deploy" command
 * should do everything required without using the prom:d command provided by
 * this class.
 *
 * The config export command is still necessary for orchestrating the moving of
 * "optional module" config changes back to the module's code base.
 */
class ProfileConfigCommands extends DrushCommands implements SiteAliasManagerAwareInterface, CustomEventAwareInterface {

  use SiteAliasManagerAwareTrait;
  use CustomEventAwareTrait;

  /**
   * The profile_manager.settings configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $drupalConfig;

  /**
   * The module handler object.
   *
   * @var ModuleHandlerInterface
   */
  protected $handler;

  /**
   * The list of enabled extensions.
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $extensions;

  /**
   * The extension name of the install profile.
   *
   * @var string
   */
  protected $profile;

  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $handler, ExtensionList $extensions, string $profile) {
    $this->drupalConfig = $config_factory->get('profile_manager.settings');
    $this->handler = $handler;
    $this->extensions = $extensions;
    $this->profile = $profile;
  }

  /**
   * Run post deployment commands.
   *
   * @command profile_manager:config_export
   *
   * @aliases prom:ex
   *
   */
  public function configExport() {
    $this->run('config:export');

    $directory = $this->getProfileSyncDirectory();
    $files = scandir($directory);
    foreach ($this->getProfileOptionalModuleConfigDirs() as $dir) {
      $config = scandir($dir) ?? [];
      foreach ($config as $config_file_name) {
        if (in_array($config_file_name, $files) && file_exists("{$directory}/$config_file_name") && is_file("{$directory}/$config_file_name")) {
          copy("{$directory}/$config_file_name", "$dir/$config_file_name");
          unlink("{$directory}/$config_file_name");
          $contents = file_get_contents("$dir/$config_file_name");
          $yaml = Yaml::decode($contents);
          // @todo add a parameter to the command to remove _core or not.
          unset($yaml['_core']);
          $dir_parts = explode(DIRECTORY_SEPARATOR, $dir);
          $dir_parts = array_reverse($dir_parts);
          $module = $dir_parts[2];
          $extension = $this->extensions->get($module);
          if (!empty($extension->info['profile_manager']['enforce_module_config']) && $extension->info['profile_manager']['enforce_module_config']) {
            if (empty($yaml['dependencies']['enforced']['module'])) {
              $yaml['dependencies']['enforced']['module'][] = $module;
            }
            elseif (array_search($module, $yaml['dependencies']['enforced']['module']) === FALSE) {
              $yaml['dependencies']['enforced']['module'][] = $module;
            }
          }
          file_put_contents("$dir/$config_file_name", Yaml::encode($yaml));
        }
      }
    }
    // Remove user configurable modules from core.extension.yml.
    $extensions = file_get_contents($directory . DIRECTORY_SEPARATOR . 'core.extension.yml');
    $extensions = Yaml::decode($extensions);
    foreach (array_keys($this->drupalConfig->get('allowed_modules')) as $module_name) {
      unset($extensions['module'][$module_name]);
    }
    file_put_contents($directory . DIRECTORY_SEPARATOR . 'core.extension.yml', Yaml::encode($extensions));
  }

  /**
   * Imports configuration for the profile and any optional modules.
   *
   * @command profile_manager:deploy
   *
   * @aliases prom:d
   *
   * @todo Add code that handles optional module dependency configs.
   */
  public function deploy() {
    // We can't just run a drush deploy because CMI expects all module config
    // to be available in the config sync directory all the time. In the real
    // world, install profiles often have optional modules, and this command
    // identifies those, and imports the installation config to the sync
    // directory for any optional module that's enabled in the install.
    $profile_dir = $this->getProfileSyncDirectory();
    $unlink = [];
    foreach ($this->getProfileOptionalModuleConfigDirs() as $dir) {
      $config = scandir($dir) ?? [];
      foreach ($config as $config_file_name) {
        if (in_array($config_file_name, ['.', '..'])) {
          continue;
        }
        copy("$dir/$config_file_name", "$profile_dir/$config_file_name");
        $unlink[] = "$profile_dir/$config_file_name";
      }
    }
    // Try/catch the config import so we can unlink config files we copied
    // regardless of import outcome.
    try {
      $this->run('deploy');
    }
    catch (\Exception $e) {}
    foreach ($unlink as $file) {
      unlink($file);
    }
  }

  protected function getProfileOptionalModulePaths(): array {
    $paths = [];
    $allowed_modules = $this->drupalConfig->get('allowed_modules') ?? [];
    foreach (array_keys($allowed_modules) as $module_name) {
      if (!$this->handler->moduleExists($module_name)) {
        continue;
      }
      $extension = $this->handler->getModule($module_name);
      $paths[] = realpath($extension->getPath());
    }
    return $paths;
  }

  protected function getProfileOptionalModuleConfigDirs(): array {
    $dirs = [];
    $modules = $this->getProfileOptionalModulePaths();
    foreach ($modules as $module) {
      $module = realpath($module);
      foreach (['install', 'optional'] as $subdir) {
        $dir = "$module/config/$subdir";
        if (!file_exists($dir)) {
          continue;
        }
        $dirs[] = $dir;
      }
    }
    return $dirs;
  }

  protected function getProfileDirectory(): Extension {
    return $this->handler->getModule($this->profile);
  }

  protected function getProfileSyncDirectory(): string {
    $profile_directory = $this->getProfileDirectory();
    $directory = realpath($profile_directory->getPath() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'sync');
    if (!$directory) {
      throw new \Exception(sprintf("The specified directory: %s could not be found.", $profile_directory->getPath()));
    }
    return $directory;
  }

  /**
   * Run the drush command.
   *
   * Display the output if the is verbose (-v).
   *
   * Error output is already displayed.
   *
   * @param $command
   * @param array $args
   * @param array $options
   */
  protected function run($command, $args = [], $options = []) {
    $io = $this->io();
    $io->writeln(sprintf('Invoking command: %s', $command));
    $options = array_merge_recursive([
      'verbose' => $io->isVerbose(),
      'debug' => $io->isDebug(),
    ], $options);
    $self = $this->siteAliasManager()->getSelf();
    /** @var \Consolidation\SiteProcess\SiteProcess $process */
    $process = $this->processManager()->drush($self, $command, $args, $options);
    $process->setRealtimeOutput($this->io(), $this->io());
    $process->mustRun($process->showRealtime());
  }
}
