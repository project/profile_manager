<?php

namespace Drupal\profile_manager;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Symfony\Component\Routing\Route;

interface ProfileManagerInterface extends PluginInspectionInterface, ConfigurableInterface {

  public function transformRoute(Route $route): Route;

  public function getTransformedRouteName(): string;

  public function getTitle(Route $route): string;

  public function getDescription(Route $route): string;

  public function getLinkParent(): string;

}
