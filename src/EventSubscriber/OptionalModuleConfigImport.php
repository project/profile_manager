<?php

namespace Drupal\profile_manager\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Serialization\Yaml;
use Drupal\profile_manager\OptionalModulesTrait;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ensures optional module configurations are imported during a config import.
 */
class OptionalModuleConfigImport implements EventSubscriberInterface {

  use OptionalModulesTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform', 100];
    return $events;
  }

  /**
   * Ensure that the optional module config is imported.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    foreach ($this->getProfileOptionalModulePaths() as $module_path) {
      $config = $module_path . DIRECTORY_SEPARATOR . "config";
      $directories = [
        $config . DIRECTORY_SEPARATOR . "install",
        $config . DIRECTORY_SEPARATOR . "optional",
      ];
      foreach ($directories as $dir) {
        if (!file_exists($dir)) {
          continue;
        }
        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir));
        foreach ($iterator as $file) {
          if ($file->isDir()){
            continue;
          }
          $info = pathinfo($file->getPathname());
          if (!empty($info['extension']) && $info['extension'] === 'yml') {
            $config = Yaml::decode(file_get_contents($file->getPathname()));
            $storage->write($info['filename'], $config);
          }
        }
      }
    }

    $profile_path = $this->extensionPathResolver->getPath('module', $this->installProfile);
    if (!is_dir($profile_path . '/config/sync')) {
      return;
    }
    $profile_storage = new FileStorage($profile_path . '/config/sync', StorageInterface::DEFAULT_COLLECTION);
    $profile_extensions = $profile_storage->read('core.extension');
    foreach ($this->getProfileOptionalModuleDependencyPaths() as $module => $module_path) {
      if (!isset($profile_extensions['module'][$module])) {
        foreach ([InstallStorage::CONFIG_INSTALL_DIRECTORY, InstallStorage::CONFIG_OPTIONAL_DIRECTORY] as $directory) {
          if (is_dir($module_path . '/' . $directory)) {
            $module_storage = new FileStorage($module_path . '/' . $directory, StorageInterface::DEFAULT_COLLECTION);
            foreach ($module_storage->listAll() as $config_name) {
              $storage->write($config_name, $module_storage->read($config_name));
            }
          }
        }
      }
    }
  }

}
