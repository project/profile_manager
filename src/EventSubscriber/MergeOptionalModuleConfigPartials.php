<?php

namespace Drupal\profile_manager\EventSubscriber;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Serialization\Yaml;
use Drupal\profile_manager\OptionalModulesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Manages config partials application during config import/export.
 *
 * ProfileManager provides a notion of "config partials". If a "partial"
 * directory is found within a module's config dir, we do name matching on
 * config object during import and merge our changes with the config. This is
 * especially useful for things like user roles where one module may provide
 * new node bundles and wants to control what roles can use various CRUD ops.
 *
 * This will also strip the partials out during config export to maintain the
 * config defined by the profile.
 */
class MergeOptionalModuleConfigPartials implements EventSubscriberInterface {

  use OptionalModulesTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform', 1000];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform', 10];
    return $events;
  }

  /**
   * Ensure that enabled optional modules config partials are merged.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    foreach ($this->getProfileOptionalModulePaths() as $module_path) {
      $dir = $module_path . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "partial";
      if (!file_exists($dir)) {
        continue;
      }
      foreach (scandir($dir) as $file_name) {
        $info = pathinfo($file_name);
        if ($info['extension'] === 'yml' && $config = $storage->read($info['filename'])) {
          $config = NestedArray::mergeDeep($config, Yaml::decode(file_get_contents($dir . DIRECTORY_SEPARATOR . $file_name)));
          $storage->write($info['filename'], $config);
        }
      }
    }
  }

  /**
   * Do not export optional module partials into profile config.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    foreach ($this->getProfileOptionalModulePaths() as $module_path) {
      $dir = $module_path . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "partial";
      if (!file_exists($dir)) {
        continue;
      }
      foreach (scandir($dir) as $file_name) {
        $info = pathinfo($file_name);
        if ($info['extension'] === 'yml' && $config = $storage->read($info['filename'])) {
          $diff = $this->diffValuesRecursive($config, Yaml::decode(file_get_contents($dir . DIRECTORY_SEPARATOR . $info['filename'] . '.yml')));
          $storage->write($info['filename'], $diff);
        }
      }
    }
  }

  /**
   * Strips values of array2 from array1.
   *
   * This is mostly a clone of the code found in DiffArray::diffAssocRecursive.
   * However, the core provided utility class does not differentiate on numeric
   * keys, so comparisons only happen at the key level and not the string
   * level. This modification of the code allows us to array_search particular
   * strings out of arrays for more accurate matching.
   *
   * @param array $array1
   * @param array $array2
   *
   * @return array
   */
  protected function diffValuesRecursive(array $array1, array $array2): array {
    $difference = [];

    foreach ($array1 as $key => $value) {
      if (is_array($value)) {
        if (!array_key_exists($key, $array2) || !is_array($array2[$key])) {
          $difference[$key] = $value;
        }
        else {
          $new_diff = $this->diffValuesRecursive($value, $array2[$key]);
          if (!empty($new_diff)) {
            $difference[$key] = $new_diff;
          }
        }
      }
      elseif(is_numeric($key) && array_search($value, $array2) === FALSE) {
        $difference[] = $value;
      }
      elseif (!is_numeric($key) && (!array_key_exists($key, $array2) || $array2[$key] !== $value)) {
        $difference[$key] = $value;
      }
    }

    return $difference;
  }

}
