<?php

namespace Drupal\profile_manager\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MaintainConfigIgnoreStar implements EventSubscriberInterface {

  public function __construct(protected StorageInterface $storage, protected ModuleHandlerInterface $moduleHandler) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform', 100];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform', 100];
    return $events;
  }

  public function onImportTransform(StorageTransformEvent $event) {
    if (!$this->moduleHandler->moduleExists('config_ignore')) {
      return;
    }
    $ignore = $this->storage->read('config_ignore.settings');
    if (empty($ignore['ignored_config_entities'])) {
      return;
    }
    foreach ($ignore['ignored_config_entities'] as $pattern) {
      if (str_contains($pattern, '*')) {
        [$prefix] = explode('*', $pattern);
        if ($prefix) {
          foreach ($this->storage->listAll($prefix) as $config_id) {
            $event->getStorage()->write($config_id, $this->storage->read($config_id));
          }
        }
      }
    }
  }

  public function onExportTransform(StorageTransformEvent $event) {
    if (!$this->moduleHandler->moduleExists('config_ignore')) {
      return;
    }
    $ignore = $this->storage->read('config_ignore.settings');
    if (empty($ignore['ignored_config_entities'])) {
      return;
    }
    foreach ($ignore['ignored_config_entities'] as $pattern) {
      if (str_contains($pattern, '*')) {
        [$prefix] = explode('*', $pattern);
        if ($prefix) {
          foreach ($this->storage->listAll($prefix) as $config_id) {
            $event->getStorage()->delete($config_id);
          }
        }
      }
    }
  }

}
