<?php

namespace Drupal\profile_manager\EventSubscriber;

use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Serialization\Yaml;
use Drupal\profile_manager\Config\RecursiveFileStorage;
use Drupal\profile_manager\OptionalModulesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Ensures optional module statuses are preserved during export/import.
 *
 * Optional modules allow site owners to customize their site according to
 * expectations set by the profile. Optional modules should never end up in
 * core.extension exports, and during the import process, they should not be
 * uninstalled. This class preserves their status on import, and excludes them
 * from the core.extension config during export.
 */
class HandleOptionalModulesCoreExtensions implements EventSubscriberInterface {

  use OptionalModulesTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::STORAGE_TRANSFORM_IMPORT][] = ['onImportTransform', 100];
    $events[ConfigEvents::STORAGE_TRANSFORM_EXPORT][] = ['onExportTransform', 100];
    return $events;
  }

  /**
   * Ensure that enabled optional modules remain enabled.
   */
  public function onImportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    $extensions = $storage->read('core.extension');
    foreach ($this->getProfileOptionalModules() as $module) {
      $extensions['module'][$module] = 0;
    }
    foreach (array_keys($this->getProfileOptionalModuleDependencyPaths()) as $module) {
      if (!isset($extensions['module'][$module])) {
        $extensions['module'][$module] = 0;
      }
    }
    $storage->write('core.extension', $extensions);
  }

  /**
   * Do not export site specific optional modules or config.
   */
  public function onExportTransform(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    $extensions = $storage->read('core.extension');
    foreach ($this->getProfileOptionalModules() as $module) {
      unset($extensions['module'][$module]);
    }
    // compare core.extensions to the profile's dependencies to exclude
    // dependencies introduced by the optional modules.
    $profile_path = $this->extensionPathResolver->getPath('module', $this->installProfile);
    if (!is_dir($profile_path . '/config/sync')) {
      $storage->write('core.extension', $extensions);
      return;
    }
    $profile_storage = new FileStorage($profile_path . '/config/sync', StorageInterface::DEFAULT_COLLECTION);
    $profile_extensions = $profile_storage->read('core.extension');
    $module_paths = $this->getProfileOptionalModuleDependencyPaths();
    foreach ($module_paths as $module => $module_path) {
      // If the module is not part of the profile's modules, unset it.
      if (!isset($profile_extensions['module'][$module])) {
        unset($extensions['module'][$module]);
      }
      // If the module is part of the profile, we don't want to do anything
      // with its config, so we should remove it from the paths.
      else {
        unset($module_paths[$module]);
      }
    }
    // Create a new file storage object for each optional module dependency's config.
    $module_storages = [];
    foreach ($module_paths as $path) {
      foreach ([InstallStorage::CONFIG_INSTALL_DIRECTORY, InstallStorage::CONFIG_OPTIONAL_DIRECTORY] as $directory) {
        if (is_dir($path . '/' . $directory)) {
          $module_storages[] = new RecursiveFileStorage($path . '/' . $directory, StorageInterface::DEFAULT_COLLECTION);
        }
      }
    }
    // If the module isn't part of the profile, remove its config.
    foreach ($module_storages as $module_storage) {
      foreach ($module_storage->listAll() as $config_name) {
        if (!$profile_storage->exists($config_name)) {
          $storage->delete($config_name);
        }
      }
    }
    $storage->write('core.extension', $extensions);
  }

}
