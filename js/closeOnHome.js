(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.profile_manager_home = {
    attach: function attach(context) {
      // Get the active tab first so we don't break it when setting defaults.
      let tab = Drupal.toolbar.models.toolbarModel.get('activeTab');
      // Set up our defaults which orient the tray vertically and remove the
      // option to change it to horizontal.
      Drupal.toolbar.models.toolbarModel.set('orientation', 'vertical');
      Drupal.toolbar.models.toolbarModel.set('isTrayToggleVisible', false);
      // Check to see if the active tab WAS null and set it back to null since
      // setting the defaults above will force it to the first tab.
      if (tab == null) {
        Drupal.toolbar.models.toolbarModel.set({activeTab: null});
      }
      // Set the active tab to null when a user clicks "Back to site" to close
      // out the administration and present the site as vanilla as possible.
      $('#toolbar-item-profile-manager-tray .toolbar-icon-escape-admin').click(function () {
        Drupal.toolbar.models.toolbarModel.set({activeTab: null});
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
