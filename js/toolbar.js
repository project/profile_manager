(function ($, Drupal, drupalSettings) {
  var options = $.extend({
    strings: {
      horizontal: Drupal.t('Horizontal orientation'),
      vertical: Drupal.t('Vertical orientation')
    }
  });
  Drupal.profile_manager = {
    views: {},
    MenuVisualView: {},
    setSubtrees: new $.Deferred(),
  };
  Drupal.behaviors.profile_manager = {
    attach: function attach(context) {
      let menuModel = new Drupal.toolbar.MenuModel();
      Drupal.profile_manager.views.profileManagerVisualView = new Drupal.profile_manager.MenuVisualView({
        el: $(context).find('.profile-manager-toolbar-menu-administration').get(0),
        model: menuModel,
        strings: options.strings
      });
      Drupal.profile_manager.setSubtrees.done(function (subtrees) {
        menuModel.set('subtrees', subtrees);
        var theme = drupalSettings.ajaxPageState.theme;
        localStorage.setItem("Drupal.profile_manager.subtrees.".concat(theme), JSON.stringify(subtrees));
        model.set('areSubtreesLoaded', true);
      });
      Drupal.profile_manager.views.profileManagerVisualView.render();
    }
  };
})(jQuery, Drupal, drupalSettings);
